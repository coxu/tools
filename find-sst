#!/usr/bin/env python3
# Dump the responsible SSTs from a commit
# find-sst <path to owners.yaml> < <commits file>

import fnmatch
import re
import sys
import yaml
import subprocess
from typing import Iterable

######################################################################################################
# Stolen from kernel-webhooks


def get_nested_key(data, key, default=None, *, lookup_attrs=False, delimiter="/"):
    """
    Dig through nested dictionaries/lists to get the value of a key.

    Inputs a key slash-separated like key_a/key_b/key_c, and returns 'value' on
    the following chain: {'key_a': {'key_b': {'key_c': value}}} handling
    missing keys.

    The default delimiter is / to play well with dot-prefix "internal" keys used
    in quite some places.

    Setting lookup_attrs=True will also retrieve object attributes.
    """
    subkeys = key.split(delimiter)
    for subkey in subkeys:
        try:
            data = data[int(subkey)] if isinstance(data, list) else data[subkey]
        except (KeyError, TypeError, IndexError, ValueError):
            if not lookup_attrs:
                return default
            try:
                data = getattr(data, subkey)
            except AttributeError:
                return default

    return data


class Entry:
    """Represents an entry in the owners.yaml file."""

    def __init__(self, entry):
        """Save an individual entry from the Parser class."""
        self._entry = entry

    @property
    def subsystem_name(self):
        """Return the subsystem name as a string."""
        return get_nested_key(self._entry, "subsystem", "")

    @property
    def subsystem_label(self):
        """Return the subsystem label as a string."""
        return get_nested_key(self._entry, "labels/name", "")

    @property
    def devel_sst(self) -> str:
        """Return the devel SST as a string"""
        ret = get_nested_key(self._entry, "devel-sst", "")
        if isinstance(ret, (list, tuple)):
            ret = ret[0]
        assert isinstance(ret, str)
        return ret

    @property
    def ready_for_merge_label_deps(self):
        """Return the list of ready for merge dep labels, if any."""
        return get_nested_key(self._entry, "labels/readyForMergeDeps", [])

    @property
    def status(self):
        """Return the status."""
        return get_nested_key(self._entry, "status", "")

    @property
    def required_approvals(self):
        """Return the requiredApproval value if set, otherwise False."""
        return get_nested_key(self._entry, "requiredApproval", False)

    @property
    def maintainers(self):
        """Return the list of maintainers."""
        if not (users := get_nested_key(self._entry, "maintainers")):
            users = []
        return list(_flatten(users))

    @property
    def reviewers(self):
        """Return the list of reviewers."""
        if not (users := get_nested_key(self._entry, "reviewers")):
            users = []
        return list(_flatten(users))

    @property
    def scm(self):
        """Return the SCM string."""
        return get_nested_key(self._entry, "scm", "")

    @property
    def mailing_list(self):
        """Return the mailinglist."""
        return get_nested_key(self._entry, "mailingList", "")

    @property
    def path_include_regexes(self):
        """Return the list of include path regexes."""
        if not (paths := get_nested_key(self._entry, "paths/includeRegexes")):
            paths = []
        return paths

    @property
    def path_includes(self):
        """Return the list of include paths."""
        if not (paths := get_nested_key(self._entry, "paths/includes")):
            paths = []
        return paths

    @property
    def path_excludes(self):
        """Return the list of exclude paths."""
        if not (paths := get_nested_key(self._entry, "paths/excludes")):
            paths = []
        return paths

    def _perform_match(self, filename):
        includes_match = _glob_matches(filename, self.path_includes) or _regex_matches(
            filename, self.path_include_regexes
        )
        return includes_match and not _glob_matches(filename, self.path_excludes)

    def matches(self, filenames):
        """Determine if any of the filenames match the path includes and excludes."""
        return any(self._perform_match(x) for x in filenames)

    def __repr__(self):
        """Return the subsystem name as the string repesentation of the Entry class."""
        return self.subsystem_name


class Parser:
    """YAML parser for the owners.yaml file."""

    def __init__(self, owner_yaml_contents):
        """Parse the string contents of the owners.yaml file."""
        if not owner_yaml_contents:
            self._entries = []
        else:
            owners = yaml.safe_load(owner_yaml_contents)
            self._entries = [Entry(entry) for entry in owners["subsystems"]]

    def get_all_entries(self):
        """Return all of the entries in the owners.yaml file."""
        return self._entries

    def get_matching_entries(self, filenames):
        """Return all of the matching entries in the owners.yaml file."""
        return [x for x in self._entries if x.matches(filenames)]


def _regex_matches(filename, regexes):
    """Perform a regex match against multiple patterns."""
    return any(_regex_match(filename, x) for x in regexes)


def _regex_match(filename, regex):
    if not regex:
        return True

    return re.compile(regex).search(filename) is not None


def _glob_matches(filename, patterns):
    """Perform a glob match against multiple patterns."""
    return any(glob_match(filename, x) for x in patterns)


def glob_match(filename, pattern):
    """Check whether the filename matches the given shell-style pattern.

    Unlike fnmatch.fnmatchcase, the directories are matched correctly.
    """
    # Special case: a pattern ending with a slash matches everything under
    # the given directory.
    dir_match = False
    if pattern.endswith("/"):
        pattern = pattern[:-1]
        dir_match = True

    # Split to path components.
    fn_components = filename.split("/")
    pat_components = pattern.split("/")

    # If the pattern has more components, there is no way the filename can
    # match.
    if len(fn_components) < len(pat_components):
        return False

    # Check the path components one by one.
    for fn_one, pat_one in zip(fn_components, pat_components):
        if not fnmatch.fnmatchcase(fn_one, pat_one):
            return False

    # If we're matching everything under the pattern directory, it's okay if
    # there are extra components in the path. This is a match.
    if dir_match:
        return True

    # We may have more components in the filename than in the pattern. This
    # should generally not happen. Let's be conservative and return no
    # match in such case.
    return len(fn_components) == len(pat_components)


def _flatten(lst):
    for i in lst:
        if isinstance(i, list):
            yield from _flatten(i)
        else:
            yield i


######################################################################################################


def get_commit_files(commit: str) -> Iterable[str]:
    cmd = f"git log -1 --name-only --format='' {commit}"
    p = subprocess.run(cmd, text=True, check=True, shell=True, stdout=subprocess.PIPE)
    for f in p.stdout.splitlines():
        yield f


def print_help() -> None:
    print("Usage: find-sst <owners.yaml> < <commits file>")


def main() -> None:
    if len(sys.argv) < 2:
        print_help()
        exit(1)

    owners_path = sys.argv[1]
    with open(owners_path, "r") as f:
        parser = Parser(f)

    for line in sys.stdin:
        if not line.strip():
            continue

        commit = line.split()[0]
        ssts = set(
            filter(
                lambda x: len(x),
                (
                    e.devel_sst
                    for e in parser.get_matching_entries(
                        tuple(get_commit_files(commit))
                    )
                ),
            )
        )
        print(" ".join(ssts))


if __name__ == "__main__":
    main()
